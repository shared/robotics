*** Required Package***
``` 
1. Gurobi
2. Python 3.5
3. Networkx
```

*** Run ***
```
python3 tomrpp.py path_of_config_file
python3 tomrpp.py ./cpnfiguration/config.ini
```
